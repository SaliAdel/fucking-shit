package sample;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application {

    private boolean isServer = false;
    private TextArea message = new TextArea();
    private Network connection = isServer ? creatServer() : creatClient();



    private Parent creatContent(){
        message.setPrefHeight(550);
        TextField input = new TextField();
        input.setOnAction(event -> {
            String mess = isServer ? "Server : " : "client : ";
            mess += input.getText();
            input.clear();
            message.appendText(mess + "\n");
            try{
                connection.send(mess);
            }
            catch (Exception e) {
                message.appendText("fck you \n");
            }
        });
        VBox root = new VBox(20,message,input);
        root.setPrefSize(600,600);

        return root;
    }

    @Override
    public void init() throws Exception {
        connection.startConct();;
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        primaryStage.setScene(new Scene(creatContent()));
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
        connection.closecnct();
    }

    private Server creatServer() {
        return new Server(55555,data -> {
            Platform.runLater(() -> {
                message.appendText(data.toString() + "\n");
            });
        });
    }

    private Client creatClient() {
        return new Client(55555,"127.0.0.1",data -> {
            Platform.runLater(() -> {
                message.appendText(data.toString() + "\n");
            });
        });
    }
    public static void main(String[] args) {
        launch(args);

    }
}
