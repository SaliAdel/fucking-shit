package sample;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;

public abstract class Network {
    private ConectThread conectThread = new ConectThread();

    private Consumer<Serializable> onReceiveCallback;
    public Network(Consumer<Serializable> onReceiveCallback){
        this.onReceiveCallback = onReceiveCallback;
    }
    public void startConct() throws Exception {
        conectThread.start();
    }
    public void send(Serializable data) throws Exception {
        conectThread.out.writeObject(data);

    }

    public void closecnct() throws Exception {
        conectThread.socket.close();
    }

    protected abstract boolean isServer();

    protected abstract String getIP();
    protected abstract int getport();


    private class ConectThread extends Thread{
        private Socket socket;
        private ObjectOutputStream out;

        @Override
        public void run() {
            try (ServerSocket server = isServer() ? new ServerSocket(getport()) : null;
                 Socket socket = isServer() ? server.accept() : new Socket(getIP(),getport());
                 ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
                 ObjectInputStream in = new ObjectInputStream(socket.getInputStream())) {

                this.socket = socket;
                this.out = out;
                socket.setTcpNoDelay(true);
                while (true){
                    Serializable data = (Serializable) in.readObject();
                    onReceiveCallback.accept(data);
                }
            }
            catch (Exception e) {
                onReceiveCallback.accept("oweifjowiejiewffck you fuck you");
            }
        }
    }
}
