package sample;

import java.io.Serializable;
import java.util.function.Consumer;

public class Client extends Network{
    private String ip;
    private int port;
    public Client(int port,String ip , Consumer<Serializable> onReceiveCallback) {
        super(onReceiveCallback);
        this.port = port;
        this.ip = ip;
    }

    @Override
    protected boolean isServer() {
        return false;
    }

    @Override
    protected String getIP() {
        return ip;
    }

    @Override
    protected int getport() {
        return port;
    }
}
